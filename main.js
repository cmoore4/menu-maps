var tudl = {
	menus: {
		id: 'p15140coll58',
		title: 'New Orleans Menu Collection',
		fields: ['restaurant', 'cuisine', 'location', 'date']
	}
};

var	LOUIS =  {
	base_url: 'http://louis:123/dmwebservices/index.php?q=',

	getItem: function(item, collection){
		return tudl.LOUIS.base_url + 'dmGetItemInfo/' + collection + '/' + item + 'json';
	},

	search: function(query, collection, returnFields, searchField){
		if (!searchField){searchField = 'CISOSEARCHALL';}
		return LOUIS.base_url + 
			'dmQuery/' + 
			collection + '/' +
			[searchField, query, 'all', 'and'].join('^') + '/' + //search parameters: mode, operator
			returnFields.join('^') + '/' + //returned fields
			'title/' + //sort
			'1024/1/0/0/0/0/' + //max, start, supress, doc, suggest, facets
			'json'; //return
	}
};

var GMaps = {
	markers: [],
	gmap: null,
	api: 'AIzaSyBcD50njNqriD33rRuQOA2CpcvDDXzB7dU',

	getCoord: function(address, cb, args){
		if (!args || args.length < 1){args = {};};
		$.getJSON('http://maps.googleapis.com/maps/geo?q=' +
			address +
			'key=' + GMaps.api +
			'&sensor=false&callback=?',
		function(data){
			args['lat'] = data.Placemark[0].Point.coordinates[0];
			args['lon'] = data.Placemark[0].Point.coordinates[1];
			return cb(args);
		});

	},

	init: function (startCoord) {
		var latlng = new google.maps.LatLng(startCoord.lat, startCoord.lon);
		var settings = {
			zoom: 15,
			center: latlng,
			mapTypeControl: true,
			mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
			navigationControl: true,
			navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
			mapTypeId: google.maps.MapTypeId.ROADMAP
    	}
    	GMaps.gmap = new google.maps.Map(document.getElementById("map_canvas"), settings);
    },

    addMarker: function(args){
    	var mark = new google.maps.Marker({
			position: new google.maps.LatLng(args.lat, args.lon),
			map: GMaps.gmap,
			title: args.title,
			zIndex: 4
		});
		var markInfo = new google.maps.InfoWindow({content: args.content});
		google.maps.event.addListener(mark, 'click', function() {
			markInfo.open(Gmaps.gmap, mark);
		});

    	Gmaps.markers.push(mark)
    }
}

function createMenuMap(){
	//console.log(LOUIS.search('fish', tudl.menus.id, tudl.menus.fields, ''));
	GMaps.getCoord('New Orleans, La, USA', GMaps.init, {});
}